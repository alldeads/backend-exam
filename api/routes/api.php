<?php

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('guest:api')->get('/login', function ( Request $request ) {

	 // $validatedData = $request->validate([
  //       'email'    => 'required|email',
  //       'password' => 'required',
  //   ]);

	// I always got a network error!!!


	 $data['message'] = "Test";
	 $data['errors']['email'] = "Test";
	 $data['errors']['password'] = "Test";

	return json_encode( $data );
} );

Route::middleware('guest:api')->post('/register', function ( Request $request ) {

	$sucess = true;

	if ( $request->password != $request->password_confirmation ) {
		$sucess = false;
		$data['message'] = "The given data was invalid.";
		$data['errors']['password'] = "The password confirmation does not match.";
	}

	if ( !isset($request->name) ) {
		$sucess = false;
		$data['message'] = "The given data was invalid.";
		$data['errors']['name'] = "The name field is required.";
	}

	return json_encode( $data );
} );

Route::middleware('guest:api')->get('/posts', function ( Request $request ) {

	return Post::paginate();
} );

Route::middleware('guest:api')->get('/posts/{post}', function ( $post ) {

	$posts['data'] = Post::where('title','like', $post )->get()->toArray();

	if ( count( $posts['data'] ) != 0 ) {

		return json_encode( $posts );
	}

	$posts['message'] = "No query results for model [App\\Post].";

	return $posts;
	
} );

Route::middleware('auth:api')->get('/posts/{post}/comments', function (Request $request) {

	Auth::logout();
});

Route::middleware('auth:api')->patch('/posts/{post}/comments/{comment}', function (Request $request) {

	Auth::logout();
});

Route::middleware('auth:api')->delete('/posts/{post}/comments/{comment}', function (Request $request) {

	Auth::logout();
});

Route::middleware('auth:api')->post('/posts', function (Request $request) {

	Auth::logout();
});

Route::middleware('auth:api')->patch('/posts/{post}', function (Request $request) {

	Auth::logout();
});

Route::middleware('auth:api')->delete('/posts/{post}', function (Request $request) {

	Auth::logout();
});

Route::middleware('auth:api')->get('/logout', function (Request $request) {

	Auth::logout();
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

