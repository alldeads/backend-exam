<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('posts')->insert([
            'user_id'  => 1,
            'title'    => str_random(10),
            'slug'     => str_random(10),
            'content'  => str_random(100),
            'created_at'=> null,
            'updated_at'=> null
        ]);

        DB::table('posts')->insert([
            'user_id'  => 1,
            'title'    => str_random(10),
            'slug'     => str_random(10),
            'content'  => str_random(100),
            'created_at'=> null,
            'updated_at'=> null
        ]);

        DB::table('posts')->insert([
            'user_id'  => 1,
            'title'    => str_random(10),
            'slug'     => str_random(10),
            'content'  => str_random(100),
            'created_at'=> null,
            'updated_at'=> null
        ]);

        DB::table('posts')->insert([
            'user_id'  => 1,
            'title'    => str_random(10),
            'slug'     => str_random(10),
            'content'  => str_random(100),
            'created_at'=> null,
            'updated_at'=> null
        ]);
    }
}
